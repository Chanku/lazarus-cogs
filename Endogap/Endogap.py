# This is a Cog designed for getting the Endogap for Lazarus
#
# Copyright (C) 2017 Chanku.Sapein
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  This is located in the LICENSE file
# If not, see <http://www.gnu.org/licenses/>.

"""Gets Lazarus Endorsement Gaps"""

import time
import urllib.request
import discord
from discord.ext import commands

__version__ = '1.0.1'
__author__ = 'Somyrion'
__maintainer__ = 'Chanku'

class Endogap:
    """ Retrieves endorsement gap between Coupers and Resistance """

    def __init__(self, bot):
        self.endo_gap_data = None
        self.last_endo_collection = 0
        self.bot = bot
        self.requests = 0
        self.time = int(time.time())
        self.rebel_points = ['courlany', 'loftegen', 'alunya', 'skadifron']
        self.rogue_points = ['funkadelia', 'killer_kitty', 'scum', 'chef_big_dog',
                             'aroostook_county']
        user = 'Etoarmaapitm'
        bot_string = "Lazarus Resisty Endogap Checker - Discord"
        self.user_agent = "{} ver {} ; User: {}; Author: {}; Maintainer: {}".format(bot_string,
                                                                                    __version__,
                                                                                    __maintainer__,
                                                                                    user,
                                                                                    __author__,)

    def get_nation_endorsements(self, nation_dict):
        """ Gets the endorsements of a dictionary of nations. """
        nations = nation_dict
        checked_nations = {}
        for nation in nations:
            nation_name, endos = self.get_endorsement_count(nation)
            if nation_name != nation:
                raise NameError('Nation Name {} is not {} as exected!'.format(nation_name, nation))
            checked_nations[nation] = endos

        return checked_nations

    def check_requests(self):
        """ Checks for the NS Rate Limit """
        if self.requests >= 48:
            current_time = int(time.time())
            time_difference = self.time - current_time
            if time_difference >= 28:
                time.sleep(15)
                self.requests = 0
                self.time = int(time.time())

    def get_endorsement_count(self, nation):
        """ This gets the actual endorsement count for a nation. """
        nation_url_template = ('http://www.nationstates.net/cgi-bin/api.cgi?'
                               'nation={}&q=endorsements&v=9')

        self.check_requests()
        nation_endorsements = self.send_request(nation_url_template, nation)
        self.requests += 1
        endorsements = nation_endorsements.split('<ENDORSEMENTS>')[1].split('</ENDORSEMENTS>')[0]

        endorsement_list = endorsements.split(',')
        endorsement_count = len(endorsement_list)
        return nation, endorsement_count

    def send_request(self, url, target):
        """ This function sends an API request to the NationStates Server.
        It then deals with the output and decodes it for usage elsewhere.
        If it can't decode it then it returns False."""
        url = '{}'.format(url.format(target))

        headers = {'User-Agent': self.user_agent}
        ns_request_url = urllib.request.Request(url=url, headers=headers)

        try:
            response = urllib.request.urlopen(ns_request_url)
        except urllib.error.HTTPError:
            raise
        r_content = response.read()
        response.close()
        try:
            return r_content.decode('utf-8')
        except UnicodeDecodeError:
            print(r_content)
            raise

    def _get_max_endo_count(self, nation_dict):
        """ Get the Max Endorsements of a Dictionary of Nations and Endorsements """
        max_val = max(nation_dict.values())
        for nation in nation_dict:
            if max_val == nation_dict[nation]:
                lead_endos = max_val
                leader = nation.title()
                return leader, lead_endos

    @commands.command()
    async def endogap(self):
        """ Retrieves endorsement gap between Coupers and Resistance """
        start_time = time.time()

        time_limit = 30 * 60 #Set limit to 30 minutes, in seconds.

        time_difference = start_time - self.last_endo_collection
        is_within_refresh = time_difference >= time_limit
        is_within_refresh = is_within_refresh or (self.last_endo_collection == 0)

        if (not self.endo_gap_data) or is_within_refresh:
            rebel = self.get_nation_endorsements(self.rebel_points)
            rogue = self.get_nation_endorsements(self.rogue_points)

            coup_leader, coup_lead_endos = self._get_max_endo_count(rogue)
            res_leader, res_lead_endos = self._get_max_endo_count(rebel)

            self.last_endo_collection = start_time
            self.endo_gap_data = [coup_leader, coup_lead_endos, res_leader, res_lead_endos]
            cached = ''
        else:
            coup_leader, coup_lead_endos, res_leader, res_lead_endos = self.endo_gap_data
            cached = ' (Cached)'

        result = coup_lead_endos - res_lead_endos
        embed = discord.Embed(description="Endorsement Gap", colour=0xd0a047)
        embed.add_field(name="**{}** endorsements".format(str(result)),
                        value=("between [{}](https://nationstates.net/{})"
                               "and [{}](https://nationstates.net/{})"
                              ).format(coup_leader, coup_leader, res_leader, res_leader)
                       )
        embed.set_footer(text="{}: {} | {}: {}{}".format(coup_leader, coup_lead_endos,
                                                         res_leader, res_lead_endos, cached
                                                        )
                        )

        await self.bot.say(embed=embed)

def setup(bot):
    """ Registers the cog with Red """

    bot.add_cog(Endogap(bot))
