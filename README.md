# Lazarus-Cogs
Cogs for Lazarus, the Resistance, and other personal NationStates uses. Rewritten by Chanku.Sapein

Original Author: Somyrion
Current Author: Chanku.Sapein
Version: 1.0.1

Forked and Rewritten from [Lazarus Cogs](https://github.com/Somyrion/Lazarus-Cogs/blob/master/Endogap/endogap.py)

Licensed under the terms of the GNU General Public License Version 3 and greater, all work belongs to their respective creators
